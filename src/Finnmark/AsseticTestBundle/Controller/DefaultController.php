<?php

namespace Finnmark\AsseticTestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('FinnmarkAsseticTestBundle:Default:index.html.twig', array('name' => $name));
    }
    
    /**
     * @Template()
     */
    public function testAction() {
        $data = array();
        
        return $data;
    }
}
